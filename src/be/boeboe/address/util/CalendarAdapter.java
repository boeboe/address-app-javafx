package be.boeboe.address.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Calendar;

/**
 * Adapter (for JAXB) to convert between the Calendar and the ISO 8601
 * String representation of the date such as '2012-12-03'.
 *
 * Created by boeboe on 9/3/14.
 */
public class CalendarAdapter extends XmlAdapter<String, Calendar> {

    @Override
    public Calendar unmarshal(String v) throws Exception {
        return CalendarUtil.parse(v);
    }

    @Override
    public String marshal(Calendar v) throws Exception {
        return CalendarUtil.format(v);
    }
}
